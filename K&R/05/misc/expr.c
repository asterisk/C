#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>


#define MAXOP 100
#define NUMBER '0'


int getop(char []);
void push(double);
double pop(void);


/* calc: a simple implementation for an rpn calculator. Exc 5-10 */
int main(int argc, char *argv[])
{
    int type, i;
    double op2;

    if (argc == 1)
        return 0;

    i = 1;
    while (argv[i] != NULL) {
        printf("value: \n\tstring: %s\n", argv[i]);
        type = getop(argv[i]);
        printf("type: \n\tchar: %c\n", type);
        printf("\tint:  %i\n", type);

        switch (type) {
            case NUMBER:
                push(atof(argv[i]));
                break;
            case '+':
                push(pop() + pop());
                break;
            case '*':
                push(pop() * pop());
                break;
            case '-':
                op2 = pop();
                push(pop() - op2);
                break;
            case '/':
                op2 = pop();
                if (op2 == 0.0)
                    printf("error: zero divisior\n");
                else
                    push(pop() / op2);
                break;
            default:
                printf("error: unkown command\n");
                break;
        }
        i++;
    }

    printf("\t%.8g\n", pop());


    return 0;
}


#define MAXVAL 100
int sp = 0;
double val[MAXVAL];

void push(double f)
{
    if (sp < MAXVAL)
        val[sp++] = f;
    else
        printf("error: stack full, can't push %g\n", f);
}

double pop(void)
{
    if (sp > 0)
        return val[--sp];
    else {
        printf("error: stack empty\n");
        return 0.0;
    }
}

int getop(char s[])
{
    if(isdigit(s[0]) || (s[0] == '-' && s[1] != '\0'))
        return NUMBER;
    else
        return s[0];
}
