#include <stdio.h>
#include <string.h>

#define MAXLINES 5000
#define MAXLEN 1000 

char *lineptr[MAXLINES];

int readlines(char *lineptr[], char lines[], int nlines);
void writelines(char *lineptr[], int nlines);
void qsort(char *lineptr[], int left, int right);



/* 5-7 exc */
int main()
{
    int nlines;
    char lines[MAXLINES * MAXLEN]; 
    
    if ((nlines = readlines(lineptr, lines, MAXLINES)) >= 0){
        qsort(lineptr, 0, nlines - 1);
        writelines(lineptr, nlines);
        return 0;
    } else {
        printf("error: input too big to sort\n");
        return 1;
    }
}

int getln(char *, int);

/* readlines: read input lines  */
int readlines(char *lineptr[], char lines[], int maxlines)
{
    int len, nlines;
    char line[MAXLEN];
    char *lineend;
    lineend = lines + (MAXLINES * MAXLEN);

    nlines = 0;
    while ((len = getln(line, MAXLEN)) > 0)
        if (nlines >= maxlines || lines + len >= lineend)
            return -1;
        else {
            line[len - 1] = '\0';
            strcpy(lines, line);
            lineptr[nlines++] = lines;
            lines += len;
        }
    return nlines;
}

void writelines(char *lineptr[], int nlines)
{
    while(nlines-- > 0)
        printf("%s\n", *lineptr++);
}
