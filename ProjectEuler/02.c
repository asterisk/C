#include <stdio.h>


int fib(int m)
{
    if (m == 1 || m == 0) return m;
    return (fib(m-1) + m);
}

int main()
{
    long l, ll;
    long long sum = 0;

    l = 1;
    while ((ll = fib(l++)) < 4000000)
        if (ll % 2 == 0)
            sum += ll;
    printf("%lld\n", sum);
    printf("%i", fib(2800));
    return 0;
}
