#include <stdio.h>


int main()
{
    int sum03 = 0, sum05 = 0,  sum15 = 0;
    int i;

    for(i = 0; i < 1000 ; i++){
        if (i%3 == 0 )
            sum03 += i;
        if (i%5 == 0)
            sum05 += i;
        if (i%15 == 0)
            sum15 += i;
    }
    printf("%i", sum03 + sum05 - sum15);
    return 0;
}
